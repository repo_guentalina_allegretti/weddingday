let navbar = document.querySelector(".WD-navbar")
document.addEventListener("scroll", () => {
    if (window.scrollY > 50) {
        navbar.classList.remove("WD-navbar-scroll")
    } else{
        navbar.classList.add("WD-navbar-scroll")
    }
})

let navbarNavDropdown = document.querySelector("#navbarNavDropdown")
navbarNavDropdown.addEventListener("mouseleave", () => {
    setTimeout(() => {
        navbarNavDropdown.classList.remove("show")
        dropdownShowed = false
    }, 1400)
})

let linkNav = document.querySelector("#linkNav")
navbarNavDropdown.addEventListener("touchstart", () => {
    setTimeout(() => {
        navbarNavDropdown.classList.remove("show")
        dropdownShowed = false
    }, 1800)
})

let imgAnimate = document.querySelector(".animate__backInDown")
document.addEventListener("scroll", () => {
    if (window.scrollY > 50) {
        navbar.classList.remove("animate__backInDown")
    } else{
        navbar.classList.add("animate__backInDown")
    }
})